# coding: utf-8
from flask import Flask, g
from flask_mongoengine import MongoEngine
from flask_admin import Admin
from flask_admin.contrib.mongoengine import ModelView

app = Flask(__name__)
app.config['MONGODB_SETTINGS'] = {'DB': 'my_catalog'}
app.config['SECRET_KEY'] = "asdkfjqo845j08q34jr0d9fjq045u39845ujwef"
db = MongoEngine(app)

admin = Admin(app)

from .index.views import bp as index
from .faq.views import bp as faq
from .faq.models import MenuItem, Article
from .faq.admin import ArticleAdminView

admin.add_view(ModelView(MenuItem))
admin.add_view(ArticleAdminView(Article))

app.register_blueprint(index)
app.register_blueprint(faq)



app.static_folder = 'static'

app.jinja_env.add_extension('pyjade.ext.jinja.PyJadeExtension')
app.jinja_env.add_extension('jinja2.ext.loopcontrols')
app.jinja_env.auto_reload = True

app.add_url_rule('/<path:filename>',
                 endpoint='static_subd',
                 subdomain='<subd>',
                 view_func=app.send_static_file)

app.add_url_rule('/<path:filename>',
                 endpoint='static',
                 view_func=app.send_static_file)


def is_empty(item):
    return item == [] or item == "" or item is None


def typeof(item):
    return type(item)

def days_to_years(days):
    days = int(days)
    years, days = divmod(days, 365)
    months = round(days/30)
    result = ""
    if years > 0:
        result += str(years) + " " + pytils.numeral.choose_plural(years, ("год", "года", "лет"))
    if months > 0:
        result += " " + str(months) + " " + pytils.numeral.choose_plural(months, ("месяц", "месяца", "месяцев"))
    return result


def datetime_to_readable(date):
    return pytils.dt.ru_strftime("%d %B %Y", inflected=True, date=date) + "<span></span>" + date.strftime("%H:%M")


def datetime_to_dmy(date):
    return pytils.dt.ru_strftime("%d %B %Y", inflected=True, date=date)


def current_year():
    return date.today().year


def valueate(start, iterations):
    a = []
    start = int(start)
    for i in range(iterations):
        a.append((start, start))
        start = start + 1
    return a


def split(string, delimiter):
    return string.split(delimiter)



app.jinja_env.globals.update(
    str=str,
    round=round,
    int=int,
    typeof=typeof,
    is_empty=is_empty,
    days_to_years=days_to_years,
    datetime_to_readable=datetime_to_readable,
    datetime_to_dmy=datetime_to_dmy,
    current_year=current_year,
    valueate=valueate,
    split=split,
    len=len
)


@app.context_processor
def inject_user():
#    notifications = []
#    if g.user.is_anonymous:
#        status = 0
#        notices_count = 0
#    else:
#        status = 1
#        notices_count = g.user.notices.filter(unread=True).count()
#    return {'user': g.user, 'server': 1, 'status': status, 'notices_count': notices_count}
    return {'server':1}


@app.context_processor
def inject_utility():
    return dict(env=app.config.get("ENVIRONMENT", "dev"), domain=app.config.get("SERVER_NAME"))
