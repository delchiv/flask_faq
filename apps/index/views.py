from flask import Blueprint
from flask import render_template

bp = Blueprint('index', __name__)

def is_empty(a):
    return False

@bp.route('/')
def index():
    return render_template('index.jade', show_login=True, companies={})
