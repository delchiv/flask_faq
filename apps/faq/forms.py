from flask_wtf import Form
from wtforms import widgets, StringField, TextAreaField, SubmitField
from flaskckeditor import CKEditor


class CKTextAreaWidget(widgets.TextArea):
    def __call__(self, field, **kwargs):
        kwargs.setdefault('class_', 'ckeditor')

        return super(CKTextAreaWidget, self).__call__(field, **kwargs)


class CKTextAreaField(TextAreaField):
    widget = CKTextAreaWidget()


class ArticleForm(Form):
    title = StringField("Title")
    link = StringField("Link")
    content = CKTextAreaField("Content")


class CKEditorForm(Form, CKEditor):
    title =  StringField()
    ckdemo = TextAreaField()
    submit = SubmitField('submit')