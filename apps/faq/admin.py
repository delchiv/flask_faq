from flask.ext.admin.contrib.mongoengine import ModelView
from flask_wtf import Form
from wtforms import widgets, StringField, TextAreaField


class CKTextAreaWidget(widgets.TextArea):
    def __call__(self, field, **kwargs):
        kwargs.setdefault('class_', 'ckeditor')

        return super(CKTextAreaWidget, self).__call__(field, **kwargs)


class CKTextAreaField(TextAreaField):
    widget = CKTextAreaWidget()


class ArticleAdminView(ModelView):
    form_overrides = dict(content=CKTextAreaField)

    create_template = 'edit.html'
    edit_template = 'edit.html'
