from apps import db


class MenuItem(db.Document):
    title = db.StringField(max_length=255, required=True)
    link = db.StringField(max_length=255, required=True)
    is_visible = db.BooleanField(default=False)
    parent = db.ReferenceField('MenuItem')

    def __unicode__(self):
        return '<MenuItem %s>' % self.title


class Article(db.Document):
    title = db.StringField(max_length=255, required=True)
    link = db.StringField(max_length=255, required=True)
    content = db.StringField()
    menu = db.ReferenceField(MenuItem)

