import os

from flask import Blueprint
from flask import render_template
from flask import request
from flask import jsonify

from .models import MenuItem
from .forms import CKEditorForm


STATIC_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'static'))
bp = Blueprint('faq', __name__, static_folder=STATIC_DIR)


@bp.route('/faq')
def faq(active=None):
    active = request.args.get("active", active)
    menuitems = MenuItem.objects.filter(is_visible=True, parent=None)
    try:
        active = MenuItem.objects.get(link=active).link
    except:
        if menuitems: active = menuitems[0].link
        else: active = ""

    return render_template('faq.jade', menuitems=menuitems, active=active)


@bp.route('/ckupload/', methods=['POST', 'GET'])
def ckupload():
    """file/img upload interface"""
    if request.method == "POST":
        form = CKEditorForm()
        response = form.upload(endpoint=bp)
        return response
    else:
        res = []
        for (dirpath, dirnames, filenames) in os.walk(os.path.abspath(os.path.join(STATIC_DIR, 'upload'))):
            for filename in filenames:
                res.append({"image": "/static/upload/%s" % filename})
        return jsonify(res)
