/* jshint -W069 */
$(document).ready(function(){

  // Update avatar on filepick and covert file to base64

  $("input[name='avatar']").on("change", function(){
    var preview = $(this).siblings(".preview");
    var file = $(this)[0].files[0];
    var reader  = new FileReader();
    var base64_field = $(this).siblings("input[name='avatar_base64']");
    reader.onloadend = function(){
      preview.css("background-image","url('"+reader.result+"')");
      base64_field.val(reader.result);
    };
    if (file) {
      reader.readAsDataURL(file);
    }
  });

  // Patch user profile

  $("[data-action='user_patch']").on("click", function(){
    var data = {};
    data["images"] = {};

    // Text inputs
    $(".tab-content[data-id='personal'] input[type='text'], .tab-content[data-id='personal'] input[type='hidden']").each(function(){
      if ($(this).data("select_name")) {
        var select = $("[name='"+$(this).data("select_name")+"']");
        if (select.val()) {
          data[$(this).attr("name")] = { "id": select.val(), "name": select.children("option:selected").attr("real_name") };
        }
      } else if ($(this).val()) {
        data[$(this).attr("name")] = $(this).val();
      }
    });

    // Text areas
    $(".tab-content[data-id='personal'] textarea").each(function(){
      if ($(this).val()) {
        data[$(this).attr("name")] = $(this).val();
      }
    });

    // Selects
    $(".tab-content[data-id='personal'] select").each(function(){
      if ($(this).val() && !($(this).hasClass("predictable"))) {
        data[$(this).attr("name")] = $(this).val();
      }
    });

    // Checkboxes
    $(".tab-content[data-id='personal'] input[type='checkbox'], .profile-general-bar input[type='checkbox']").each(function(){
      if ($(this).prop("checked")) {
        data[$(this).attr("name")] = true;
      } else {
        data[$(this).attr("name")] = false;
      }
    });

    // Avatar
    if ($("[name='avatar_base64']").val()) {
      data["images"]["avatar"] = $("[name='avatar_base64']").val();
    }
    $(".profile-general-bar input[type='checkbox']").each(function(){
      if ($(this).prop("checked")) {
        data[$(this).attr("name")] = true;
      } else {
        data[$(this).attr("name")] = false;
      }
    });

    console.log("pathing user with next data:");
    console.log(data);
    tugush.API.userPatch(data, function(response){
      console.log("OK. Response:");
      console.log(response);
      tugush.UI.showMessage("Данные сохранены.", function(){ location.reload(); });
    });
  });

  // Patch user security

  $("[data-action='user_patch_security']").on("click", function(){
    var data = {};

    // Text inputs
    $(".tab-content[data-id='security'] input[name='email'], .tab-content[data-id='security'] input[name='phone']").each(function(){
      if ($(this).val()) {
        data[$(this).attr("name")] = $(this).val();
      }
    });

    // Avatar
    if ($("[name='avatar_base64']").val()) {
      data["images"]["avatar"] = $("[name='avatar_base64']").val();
    }
    $(".profile-general-bar input[type='checkbox']").each(function(){
      if ($(this).prop("checked")) {
        data[$(this).attr("name")] = true;
      } else {
        data[$(this).attr("name")] = false;
      }
    });

    console.log("pathing user with next data:");
    console.log(data);
    tugush.API.userPatch(data, function(response){
      console.log("OK. Response:");
      console.log(response);
      tugush.UI.showMessage("Данные сохранены.");
    });
  });

  // Patch user password

  $("[data-action='user_change_password']").on("click", function(){

    var password_old = $(".tab-content[data-id='security'] input[name='password_old']");
    var password_new = $(".tab-content[data-id='security'] input[name='password_new']");
    var password_new_repeat = $(".tab-content[data-id='security'] input[name='password_new_repeat']");

    password_old.removeClass("error");
    password_new.removeClass("error");
    password_new_repeat.removeClass("error");

    var data = {};

    if (password_new.val() == password_new_repeat.val()) {
      if (password_new.val()) {
        data["password_old"] = password_old.val();
        data["password_new"] = password_new.val();
        console.log("pathing user with next data:");
        console.log(data);

        // Avatar
        if ($("[name='avatar_base64']").val()) {
          data["images"]["avatar"] = $("[name='avatar_base64']").val();
        }
        $(".profile-general-bar input[type='checkbox']").each(function(){
          if ($(this).prop("checked")) {
            data[$(this).attr("name")] = true;
          } else {
            data[$(this).attr("name")] = false;
          }
        });

        tugush.API.passwordPatch(data,
          function(response){
            console.log("OK. Response:");
            console.log(response);
            tugush.UI.showMessage("Пароль обновлён.");
          },
          function(XHR_obj, status, error){
            var error_fields = XHR_obj.responseJSON.fields;
            for (var idx in error_fields) {
              var field = error_fields[idx];
              console.log("wrong:");
              console.log(field);
              $("input[name='"+field+"']").addClass("error");
            }
          }
        );
      } else {
        password_new.addClass("error");
      }
    } else {
      password_new_repeat.addClass("error");
    }
  });

  // Clear password inputs

  setTimeout(function(){
    $("[name='password_old']").val("");
  }, 500);

  // Patch user notifications

  $("[data-action='user_patch_notifications']").on("click", function(){
    var data = {};

    // Checkboxes
    $(".tab-content[data-id='notifications'] input[type='checkbox']").each(function(){
      if ($(this).prop("checked")) {
        data[$(this).attr("name")] = true;
      } else {
        data[$(this).attr("name")] = false;
      }
    });

    console.log("pathing user with next data:");
    console.log(data);
    tugush.API.userPatch(data, function(response){
      console.log("OK. Response:");
      console.log(response);
      tugush.UI.showMessage("Данные сохранены.");
    });
  });

  // Add card

  $("[data-action='post_card']").on("click", function(){
    var data = {};
    var number = $("[data-id='newcard'] input[name='number']");
    number.removeClass("error");
    if (number.val().length < 16) {
      number.addClass("error");
    } else {
      data["number"] = number.val();
      tugush.API.cardsPost(data, function(response){
        console.log("response");
        tugush.UI.showMessage("Карта добавлена.", function(){ location.reload(); });
      });
    }
  });

  // Delete card

  $("[data-action='delete_card']").on("click", function(){
    var card_id = $(this).data("target");
    var row = $(this).parents(".table-row");
    tugush.API.cardsDelete(card_id, function(response){
      row.slideUp(function(){ row.remove(); });
    });
  });

  // Add bank

  $("[data-action='post_bank']").on("click", function(){
    var data = {};
    var number = $("[data-id='newbank'] input[name='bik']");
    number.removeClass("error");
    if (number.val().length < 12) {
      number.addClass("error");
    } else {
      data["number"] = number.val();
      tugush.API.banksPost(data, function(response){
        console.log("response");
        tugush.UI.showMessage("Счёт добавлен.", function(){ location.reload(); });
      });
    }
  });

  // Delete bank

  $("[data-action='delete_bank']").on("click", function(){
    var bank_id = $(this).data("target");
    var row = $(this).parents(".table-row");
    tugush.API.banksDelete(bank_id, function(response){
      row.slideUp(function(){ row.remove(); });
    });
  });


  // Message search talker close and show

  function showSearchTalkers(){
    $(".profile-events-search-talkers").fadeIn();
    $("#search_talkers_input").focus();
  }
  function hideSearchTalkers(){
    $(".profile-events-search-talkers").fadeOut();
  }
  $(".search-talkers-container .close").on("click", function(){
    hideSearchTalkers();
  });
  $(".profile-events-messages button[data-action='show_search_talkers']").on("click", function() {
    showSearchTalkers();
  });

});
