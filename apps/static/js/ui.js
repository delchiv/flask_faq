var lastScrollPosition = 0;

function positionRightpart(){
  if ($(".nav-section").length > 0) {
    var scrollPosition = $(document).scrollTop();
    var tabsSection = $(".tabs-section").position().top;
    var tabsSectionHeight = $(".tabs-section").height();
    var rightpartHeight = $(".overview_rightpart").height();
    var rightpart = $(".overview_rightpart");
    var out = 60;

    if ($(".nav-section").position().top < scrollPosition) {
      // Positioning
      if (scrollPosition > tabsSection) {
        var offset = scrollPosition - tabsSection;
        if (scrollPosition < (tabsSection + tabsSectionHeight - rightpartHeight)) {
          $(".overview_rightpart").animate({
            "margin-top" : offset + "px"
          }, 300);
        } else {
          $(".overview_rightpart").animate({
            "margin-top" : tabsSectionHeight - rightpartHeight + "px"
          }, 300);
        }
        return false;
      }
    }
    $(".overview_rightpart").animate({
      "margin-top" : "0px"
    }, 300);
  }
}

function positionNavigationLinks(){
  if ($(".nav-section").length > 0) {
    var scrollPosition = $(document).scrollTop();
    var tabsSection = $(".tabs-section").position().top;
    var tabsSectionHeight = $(".tabs-section").height();
    var rightpartHeight = $(".overview_rightpart").height();
    var rightpart = $(".overview_rightpart");
    var out = 60;

    if (scrollPosition > tabsSection - 200 && scrollPosition < tabsSection) {
       var nearness = (tabsSection - scrollPosition) / 200;
       $(".progectNavigation .progectHeaderLinks").css("transform", "translateY(" + (out * nearness + "px)"));
    } else if (scrollPosition < tabsSection - 200) {
       $(".progectNavigation .progectHeaderLinks").css("transform", "translateY(" + out + "px)");
    } else {
       $(".progectNavigation .progectHeaderLinks").css("transform", "translateY(0px)");
    }
  }
}

function handleControlQuestion(question) {
    var to_hide = question.data("to_hide");
    var to_show = question.data("to_show");

    if (question.prop("checked")) {
      $("[data-id="+to_hide+"]").slideUp();
      $("[data-id="+to_show+"]").slideDown();
    } else {
      $("[data-id="+to_hide+"]").slideDown();
      $("[data-id="+to_show+"]").slideUp();
    }
}

function updateScrollDriven() {
  requestAnimationFrame(updateScrollDriven);
  if ($(document).scrollTop() != lastScrollPosition) {
    positionNavigationLinks();
    positionRightpart();
    lastScrollPosition = $(document).scrollTop();
  }
}

$(document).ready(function(){

  updateScrollDriven();

  if ($(".nav-section").length > 0) {
    var top = $(".nav-section").position().top;
    $('.nav-section').affix({
      offset: {
        top: top
      }
    });
  }

  $(".NavigationLinksgroup > div").on("click", function(){
    if ($(document).scrollTop() > $(".tabs-section").position().top) {
      $(".overview_rightpart").css("margin-top", 0 + "px");
      $(document).scrollTop($(".tabs-section").position().top);
    }
    positionRightpart();
  });

  $(".datetime-field").datetimepicker({
    locale: 'ru',
    format: 'YYYY-MM-DD HH:mm'
  });

  $("[data-to_hide], [data-to_show]").each(function() {
    handleControlQuestion($(this));
  });

  $("[data-to_hide], [data-to_show]").on("change", function() {
    handleControlQuestion($(this));
  });

  if ($("#invested_before").length > 0) {

    if ($("#invested_before").prop("checked")) {
      $("#invested_before_block").css("display","block");
      $("#invested_before_block_2").css("display","none");
    } else {
      $("#invested_before_block").css("display","none");
      $("#invested_before_block_2").css("display","block");
    }

    $("#invested_before").on("change", function() {
      if ($(this).prop("checked")) {
        $("#invested_before_block").slideDown();
        $("#invested_before_block_2").slideUp();
      } else {
        $("#invested_before_block").slideUp();
        $("#invested_before_block_2").slideDown();
      }
    });

  }

  $(".topnav .dropdown-toggle").on("mouseenter", function(){
    if (!$(this).attr("aria-expanded") || $(this).attr("aria-expanded") == "false") {
      $(this).trigger("click");
    }
  });
  $(".menuitem.dropdown").on("mouseleave", function(){
    if ($(this).hasClass('open')) {
      $(this).trigger("click");
    }
  });
});
