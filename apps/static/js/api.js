if (typeof tugush === 'undefined') {
  var tugush = {};
}

tugush.API = {
  call: function(params){
    $.ajax({
      method: params.method,
      url: "/api/"+params.url,
      data: JSON.stringify(params.data),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    }).done(function(data){
      params.ok(data);
    }).fail(function(XHR_obj, status, error){
      if ("fail" in params) {
      }
      params.fail(XHR_obj, status, error);
    });
  },
  userPatch: function(data, ok){
    var self = this;
    self.call({
      method: "PATCH",
      url: "me",
      data: data,
      ok: ok
    });
  },
  userGet: function(ok){
    var self = this;
    self.call({
      method: "GET",
      url: "me",
      data: {},
      ok: ok
    });
  },
  cardsPost: function(data, ok){
    var self = this;
    self.call({
      method: "POST",
      url: "me/cards",
      data: data,
      ok: ok
    });
  },
  cardsGet: function(ok){
    var self = this;
    self.call({
      method: "GET",
      url: "me/cards",
      data: {},
      ok: ok
    });
  },
  cardsDelete: function(card_id, ok){
    var self = this;
    self.call({
      method: "DELETE",
      url: "me/cards/"+card_id,
      data: {},
      ok: ok
    });
  },
  banksPost: function(data, ok){
    var self = this;
    self.call({
      method: "POST",
      url: "me/banks",
      data: data,
      ok: ok
    });
  },
  banksGet: function(ok){
    var self = this;
    self.call({
      method: "GET",
      url: "me/banks",
      data: {},
      ok: ok
    });
  },
  banksDelete: function(bank_id, ok){
    var self = this;
    self.call({
      method: "DELETE",
      url: "me/banks/"+bank_id,
      data: {},
      ok: ok
    });
  },
  passwordPatch: function(data, ok, fail){
    var self = this;
    self.call({
      method: "PATCH",
      url: "me/password",
      data: data,
      ok: ok,
      fail: fail
    });
  },
  userSignOut: function(ok){
    var self = this;
    self.call({
      method: "GET",
      url: "signout",
      data: {},
      ok: ok
    });
  },
  companyCreate: function(params){
    var self = this;
    self.call({
      method: "POST",
      url: "company",
      data: params.data,
      ok: params.ok
    });
  },
  companyUpdate: function(params){
    var self = this;
    self.call({
      method: "PATCH",
      url: "company/"+params.id,
      data: params.data,
      ok: params.ok
    });
  },
  companyDelete: function(params){
    var self = this;
    self.call({
      method: "DELETE",
      url: "company/"+params.id,
      data: {},
      ok: params.ok
    });
  },
  searchUser: function(string, ok, fail){
    var self = this;
    self.call({
      method: "GET",
      url: "search/user/"+string,
      data: {},
      ok: ok,
      fail: fail
    });
  },
  investmentApprove: function(investment_id, ok){
    var self = this;
    self.call({
      method: "PATCH",
      url: "investment/"+investment_id,
      data: {approve: true},
      ok: ok
    });
  },
  investmentReject: function(investment_id, ok){
    var self = this;
    self.call({
      method: "PATCH",
      url: "investment/"+investment_id,
      data: {reject: true},
      ok: ok
    });
  }
};
