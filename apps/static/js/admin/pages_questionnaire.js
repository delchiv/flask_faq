$(document).ready(function(){

  $("[data-action='add_answer']").on("click", function() {
    var answer = $(this).parents(".form-horizontal").find(".answer:last-child").clone();
    $(this).parents(".form-horizontal").find(".answers").append(answer);
  });

  $(".answers").on("click", "[data-action='remove_answer']", function() {
    var parent = $(this).parents(".answer");
    if (parent.index() > 0) {
      parent.remove();
    }
  });

  $("[data-action='questionPost']").on("click", function() {
    var data = {};
    var question = $("#question_create");
    data["title"] = question.find("[data-name='title']").val();
    data["question_to_show"] = question.find("[data-name='question_to_show']").val();
    data["question_to_hide"] = question.find("[data-name='question_to_hide']").val();
    data["title"] = question.find("[data-name='title']").val();
    data["answers"] = [];
    question.find("[data-name='answer']").each(function() {
      data["answers"].push($(this).val());
    });
    data["is_radio"] = question.find("[data-name='is_radio']").is(":checked");
    console.log(data);
    admin.API.questionPost(data, function(mess) {
      location.reload();
    });
  });

  $('#question_edit').on('show.bs.modal', function(e) {
    var self = $(this);
    var question_id = $(e.relatedTarget).data('id');
    admin.API.questionGet(question_id, function(question) {
      self.find("[data-name='title']").val(question.title);
      self.find("[data-name='question_to_hide']").val(question.question_to_hide).trigger('change');
      self.find("[data-name='question_to_show']").val(question.question_to_show).trigger('change');
      self.find(".answer:not(:first-child)").remove();
      question.answers.forEach(function(item, idx) {
        console.log("adding answer", item, idx);
        self.find(".answer:last-child").find("input").val(item);
        if (idx != question.answers.length-1) {
          self.find(".answer:last-child").clone().appendTo(".answers");
        }
      });
    });
    $(this).find("[data-action='questionPatch']").attr('data-id', $(e.relatedTarget).data('id'));
  });

  $("[data-action='questionPatch']").on("click", function() {
    var data = {};
    var question = $("#question_edit");
    var question_id = $(this).data("id");
    data["title"] = question.find("[data-name='title']").val();
    data["question_to_show"] = question.find("[data-name='question_to_show']").val();
    data["question_to_hide"] = question.find("[data-name='question_to_hide']").val();
    data["answers"] = [];
    question.find("[data-name='answer']").each(function() {
      data["answers"].push($(this).val());
    });
    data["is_radio"] = question.find("[data-name='is_radio']").is(":checked");
    console.log(data);
    admin.API.questionPatch(question_id, data, function(mess) {
      location.reload();
    });
  });

  $("[data-action='questionDelete']").on("click", function() {
    var question_id = $(this).data("id");
    var question = $(this).parents(".content-box");
    admin.API.questionDelete(question_id, function(mess) {
      question.slideUp(function() {
        question.remove();
      });
    });
  });

});
