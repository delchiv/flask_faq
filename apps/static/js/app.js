if (typeof tugush === 'undefined') {
  var tugush = {};
}

tugush.InitBindings = function(){
  $('form#personal-account-form .savedata').on('click', function(e){
    e.preventDefault();
    var form = $('form#personal-account-form');
    var fields = form.find('input');
    console.log(fields);
  });
}

tugush.Init = function(){
  tugush.InitBindings();
}


$(document).ready(function(){

  tugush.Init();

  //new country mask

  $(function(){
    $('#phone-country').phonecode({
      preferCo: 'ru'
    });
    $('#start-options input[name="phone"]').on('keypress', function(e){
      if (!((e.which>47 && e.which<58))) return false;
      if ($(this).val().length>14) return false;
    });
  });

  // Registration form

  function send_register() {
    var form = $("#registration_user");
    var data = {};
    var fields = ["firstname", "lastname", "email", "password"];
    for (var i in fields) {
      data[fields[i]] = form.find('input.'+fields[i]).val();
    }
    tugush.API.call({
      method: "POST",
      url: "signup",
      data: data,
      ok: function(data){
        console.log(data);
        location.reload();
      }
    });
  }
  function send_register_standalone() {
    $('#registration_user_standalone .bluebutton').prevAll('.error_msg').remove();
    var form = $("#registration_user_standalone");
    var data = {};
    var fields = ["first_name", "last_name", "email", "password"];
    for (var i in fields) {
      data[fields[i]] = form.find('input.'+fields[i]).val();
    }
    if($('.autentification.single').length){
      var next_page = localStorage.getItem("next_page");
      if (next_page == "become_an_investor") {
        data["role"] = "investor";
      }
      if (next_page == "company/create") {
        data["role"] = "crowdfunder";
      }
    }
    else {
      data["role"] = $('#registration_user_standalone input[name="role"]:checked').val();
    }
    tugush.API.call({
      method: "POST",
      url: "signup",
      data: data,
      ok: function(data){
        console.log(data);

        if (localStorage.getItem("next_page")) {
          var goto = "/"+localStorage.getItem("next_page");
        } else {
          var goto = "/";
        }

        tugush.UI.showMessageAndGoTo("Регистрация прошла успешно.", goto);

      },
      fail: function(XHR_obj, status, error,msg){
        var err_msg = JSON.parse(XHR_obj['responseText'])['message'];
        $('#registration_user_standalone .bluebutton').before('<div class="error_msg">'+err_msg+'</div>')
      }
    });
  }
  function login_user() {
    $('#login_user_true .whitebutton').prevAll('.error_msg').remove();
    var form = $('#login_user_true');
    var data = {};
    var fields = ["email", "password"];
    for (var i in fields) {
      data[fields[i]] = form.find('input.'+fields[i]).val();
    }
    tugush.API.call({
      method: "POST",
      url: "signin",
      data: data,
      ok: function(data){
        console.log(data);
        location.reload();
      },
      fail: function(XHR_obj, status, error,msg){
        var err_msg = JSON.parse(XHR_obj['responseText'])['message'];
        $('#login_user_true .whitebutton').before('<div class="error_msg">'+err_msg+'</div>')
      }
    });
  }

  //registration validate
  $("#registration_user_standalone").validate({
    submitHandler: function(form) {
      send_register_standalone();
    },
    errorPlacement: function(error, element) {
      error.appendTo('#errordiv');
    },
    ignore: [],
    rules: {
      firstname: {
        required: true,
        minlength: 1
      },
      lastname: {
        required: true,
        minlength: 1
      },
      password: {
        required: true,
        minlength: 5
      },
      confirm_password: {
        required: true,
        minlength: 5,
        equalTo: "#registration_user_standalone input[name='password']"
      },
      email: {
        required: true,
        email: true
      },
      role: {
        required: true,
      },
      agreement: {
        required: true,
      }
    },
    messages: {
      firstname: "Пожалуйста, введите Ваше имя",
      lastname: "Пожалуйста, введите Вашу Фамилию",
      // firstname: {
      //   required: "Пожалуйста, введите Ваше имя",
      //   minlength: "Your username must consist of at least 2 characters"
      // },
      password: {
        required: "Пожалуйста, введите Ваш пароль",
        minlength: "Пароль должен содержать не менее 5 символов"
      },
      confirm_password: {
        required: "Пожалуйста, подтвердите Ваш пароль",
        minlength: "Пароль должен содержать не менее 5 символов",
        equalTo: "Введенные пароли не совпадают"
      },
      email: "Пожалуйста, введите корректный email",
      role: "Пожалуйста выберете свою роль",
      agreement: "Примите условия соглашения"
    },
    onkeyup: function(element) {
      $(element).siblings('.error').remove();
      $(element).valid();
    },
    errorPlacement: function (error, element) {
      if(element.val().length == 0) {
        error.insertAfter(element);
      }
      error.click(function(event) {
        element.focus();
        error.remove();
      });
    }
  });

  //login validate
  $("#login_user_true").validate({
    submitHandler: function(form) {
      login_user();
    },
    errorPlacement: function(error, element) {
      error.appendTo('#errordiv');
    },
    ignore: [],
    rules: {
      password: {
        required: true,
      },
      email: {
        required: true,
        email: true
      },
    },
    messages: {
      password: {
        required: "Пожалуйста, введите Ваш пароль",
      },
      email: "Пожалуйста, введите корректный email",
    },
    onkeyup: function(element) {
      $(element).siblings('.error').remove();
      $(element).valid();
    },
    errorPlacement: function (error, element) {
      if(element.val().length == 0) {
        error.insertAfter(element);
      }
      error.click(function(event) {
        element.focus();
        error.remove();
      });
    }
  });

  // Create company

  $(".create-company .next-page").on("click", function(e){
    e.preventDefault();

    // Prepare data
    var data = {};
    var comp_name = $(this).siblings(".input-container").find("input[name='company-name']");
    if(!comp_name.val()){
      comp_name.next('label').remove();
      comp_name.addClass('error');
      comp_name.after('<label class="error">Введите название компании</label>');
      return false;
    }
    data["company_type"] = $(".create-company-active").data("id");
    data["name"] = comp_name.val();
    var checks = [];
    $(".create-company-active").parents().eq(0).next().find("input[type='checkbox']:checked").each(function(index){
      checks.push($(this).data("id"));
    });
    data["company_type_checks"] = checks;

    // Create base company and redirect to editor
    tugush.API.companyCreate({
      data: data,
      ok: function(msg){
        location.href = "/company/"+msg.name_translit+"/edit";
      }
    });
  });

  // Edit company

  $(".company-save").on("click", function(e){
    e.preventDefault();

    // Prepare data
    var data = {};
    loc = location.href.split("/");
    data["id"] = $(this).data("id");
    var images = {};
    $(".base64-image-field").each(function(){
      if ($(this).val()) {
        images[$(this).attr("name")] = $(this).val();
      }
    });
    data["images"] = images;

    $(".string-field, .int-field, .datetime-field").each(function(){
      if ($(this).val()) {
        data[$(this).attr("name")] = $(this).val();
      }
    });

    type_options = {};
    $(".type-select-field, .type-int-field").each(function(){
      if ($(this).val()) {
        type_options[$(this).attr("name")] = {};
        type_options[$(this).attr("name")]["value"] = $(this).val();
      }
    });
    $(".type-range-field").each(function(){
      if ($(this).val()) {
        if (!($(this).attr("name") in type_options)) {
          type_options[$(this).attr("name")] = {};
        }
        type_options[$(this).attr("name")][$(this).data("rangepoint")] = $(this).val();
      }
    });
    data["type_options"] = type_options;

    // Update company data
    tugush.API.companyUpdate({
      data: data,
      id: data["id"],
      ok: function(msg){
        tugush.UI.showMessage("Данные успешно сохранены.", function(){
          $('html,body').animate({ scrollTop: 0 }, 'slow');
        });
        console.log(msg);
      }
    });
  });


  // Edit company

  $(".editor-save").on("click", function(e){
    e.preventDefault();

    // Prepare data
    var data = {};
    data["id"] = $(this).data("id");
    var textarea = $(this).parents(".editor-container").find("textarea");
    var key = textarea.attr("name");
    data[key] = textarea.val();

    // Update company data
    tugush.API.companyUpdate({
      data: data,
      id: data["id"],
      ok: function(msg){
        tugush.UI.showMessage("Данные успешно сохранены.", function(){});
        console.log(msg);
      }
    });
  });

  // Change company status

  $(".company-change-status").on("click", function(e){
    e.preventDefault();
    var self = $(this);
    var err = 0;
    $('input[name="name_business"], input[name="main_word"],textarea[name="summary"]').on('input', function(event) {
      var thh = $(this);
      if(thh.val()){
        thh.next('label').remove();
        thh.removeClass('error');
      }
      else {
        thh.addClass('error');
        thh.after('<label class="error">Заполните данное поле</label>');
      }
    });
    $('input[name="name_business"], input[name="main_word"],textarea[name="summary"]').each(function(index, el) {
      var thh = $(this);
      thh.next('label').remove();
      if(!thh.val()){
        thh.addClass('error');
        thh.after('<label class="error">Заполните данное поле</label>');
         err = 1;
      }
    });

    if(err == 1){
      var target = ($('input[name="name_business"]').offset().top)-95;
      $('html, body').animate({scrollTop:target}, 800);
      return false;
    }
    // Prepare data
    var data = {};
    data["id"] = $(this).data("id");
    data["status"] = $(this).attr("data-target-status");

    // Update company data
    tugush.API.companyUpdate({
      id: data["id"],
      data: data,
      ok: function(msg){
        if (self.attr("data-target-status") == "draft") {
          self.attr("data-target-status", "published");
          self.text("Подтвердить компанию");
        } else {
          self.attr("data-target-status", "draft");
          self.text("Скрыть компанию");
          tugush.UI.showMessageAndGoTo("Спасибо! Компания подтверждена. Вы будете перенаправлены.", "/profile");
        }
        console.log(msg);
      }
    });
  });

  // Update logo and filepicker states

  $(".create-company input[type='file']").on("change", function(){
    var preview = $(this).parents(".download-pic").find(".pic-in-round");
    var file = $(this)[0].files[0];
    var reader  = new FileReader();
    var base64_field = $(this).siblings(".base64-image-field");
    reader.onloadend = function(){
      if (preview){
        preview.css("background-image","url('"+reader.result+"')");
      }
      base64_field.val(reader.result);

    }
    if (file) {
      reader.readAsDataURL(file);
    }
    $(this).siblings(".buttonMask").text("Файл выбран");
  });


  //test investor

  //function send test investor
  function send_test_investor() {
    var data = {};
    data["first_name"] = $(".form-field[name='first_name']").val();
    data["last_name"] = $(".form-field[name='last_name']").val();
    data["p_name"] = $(".form-field[name='p_name']").val();

    data["phone"] = $('.country-phone-selected').text()+''+$(".form-field[name='phone']").val();
    data["age"] = $(".form-field[name='age']").val();
    if ($("#invested_before").prop("checked")) {
      data["invested_before"] = true;
    }
    $(".questionnaire-checkbox").each(function(index){
      if ($(this).prop("checked")){
        var name = $(this).attr("name");
        if (!(name in data)) {
          data[name] = [];
        }
        data[name].push($(this).val())
      }
    });
    tugush.API.userPatch({"questionnaire": data}, function(){
      // tugush.UI.showMessageAndGoTo("Спасибо, ваша заявка принята! Вы будете перенаправлены.", "/agreement1");
      location.href="/questionnaire_true";
    });
  }
  //validate test investor
  $('#start-options').validate({
    submitHandler: function(form) {
      send_test_investor();
    },
    errorPlacement: function(error, element) {
      error.appendTo('#errordiv');
    },
    ignore: [],
    rules: {
      first_name: {
        required: true,
        minlength: 1
      },
      last_name: {
        required: true,
        minlength: 1
      },
      sur_name: {
        required: true,
        minlength: 1
      },
      age: {
        required: true,
      },
      phone: {
        required: true,
      },
      invested_before: {
        required: true,
      },
      investment: {
        required: true
      },
      favorite_categories: {
        required: true
      },
      last_year_profit: {
        required: true
      },
      favorite_profit: {
        required: true
      },
      current_state: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
    },
    messages: {
      first_name: "Пожалуйста, введите Ваше имя",
      last_name: "Пожалуйста, введите Вашу Фамилию",
      email: "Пожалуйста, введите корректный email",
      phone: "Пожалуйста, введите телефон",
      age: "Пожалуйста, введите возраст",
      invested_before: "Обязательно",
      investment: "Обязательно",
      favorite_categories: "Обязательно",
      last_year_profit: "Обязательно",
      favorite_profit: "Обязательно",
      current_state: "Обязательно",
    },
    onkeyup: function(element) {
      $(element).siblings('.error').remove();
      $(element).valid();
    },
    errorPlacement: function (error, element) {
      if(element.val().length == 0) {
        error.insertAfter(element);
      }
      error.click(function(event) {
        element.focus();
        error.remove();
      });
    }
  });

  if($('#start-options input[name="age"]').length){
    $('#start-options input[name="age"]').mask("9?99");
  }

  $('input[name="current_account"]').mask("40899999999999999999");
  $('input[name="bik"]').mask("99-99-99-999");

  //инвестор соглашение первый шаг валидация
  $('.agreementstep1 a.next-page').on('click', function(event) {
    event.preventDefault();
    var data = {};
    data["investor_type"] = $('.agreement-active').data('type');
    tugush.API.userPatch(data, function(){
      tugush.UI.showMessageAndGoTo("Данные приняты. Вы будете перенаправлены.", "/agreement2");
    });
  });

  //инвестор соглашение второй шаг
  function agreement2() {
    var formarr = $('#agreement-individ').serialize().split('&');
    var data = {},
    data2 = {};
    for (var i = 0; i < formarr.length; i++) {
      var valspl = formarr[i].split('=');
      if(valspl[1]){
        data[valspl[0]] = decodeURIComponent(valspl[1]);
      }
    }
    // tugush.API.call({
    //   method: "POST",
    //   url: "sendsms",
    //   data: {"phone" : $('input[name="phone"]').val()},
    //   ok: function(data){
    //   }
    // });
    tugush.API.userPatch({"personal_info": data}, function(){
      tugush.UI.showMessageAndGoTo("Ваши данные приняты. Вы будете перенаправлены.", "/agreement3");
    });
  }
  $('#agreement-individ').validate({
    submitHandler: function(form) {
      agreement2();
    },
    errorPlacement: function(error, element) {
      error.appendTo('#errordiv');
    },
    ignore: [],
    rules: {
      ind_name: {
        required: true,
        minlength: 1
      },
      ind_surname: {
        required: true,
        minlength: 1
      },
      ind_patronymic: {
        required: true,
      },
      ind_inn: {
        required: true,
      },
      passport_series: {
        required: true,
      },
      passport_number: {
        required: true
      },
      issued_by: {
        required: true
      },
      when_issued: {
        required: true
      },
      sity: {
        required: true
      },
      street: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      home: {
        required: true
      },
      housing: {
        required: true
      },
      apartment: {
        required: true
      },
      index: {
        required: true
      },
      bik: {
        required: true
      },
      loro_account: {
        required: true
      },
      current_account: {
        required: true
      },
      phone: {
        required: true
      },
    },
    messages: {
      ind_name: "Пожалуйста, введите Ваше имя",
      ind_surname: "Пожалуйста, введите Вашу Фамилию",
      ind_patronymic: "Пожалуйста, введите Вашe Отчество",
      email: "Пожалуйста, введите корректный email",
      ind_inn: "Пожалуйста, введите ИНН",
      passport_series: "Пожалуйста, введите cерию и номер паспорта",
      passport_number: "Пожалуйста, введите код подразделения",
      issued_by: "Пожалуйста, заполните данное поле",
      when_issued: "Пожалуйста, заполните данное поле",
      sity: "Пожалуйста, заполните данное поле",
      street: "Пожалуйста, заполните данное поле",
      home: "Пожалуйста, заполните данное поле",
      housing: "Пожалуйста, заполните данное поле",
      apartment: "Пожалуйста, заполните данное поле",
      index: "Пожалуйста, заполните данное поле",
      bik: "Пожалуйста, заполните данное поле",
      loro_account: "Пожалуйста, заполните данное поле",
      current_account: "Пожалуйста, заполните данное поле",
      phone: "Пожалуйста, заполните данное поле",
    },
    onkeyup: function(element) {
      $(element).siblings('.error').remove();
      $(element).valid();
    },
    errorPlacement: function (error, element) {
      if(element.val().length == 0) {
        error.insertAfter(element);
      }
      error.click(function(event) {
        element.focus();
        error.remove();
      });
    }
  });

  $("#codeVerification").validate({
    submitHandler: function(form) {
      var data = {};
      data["code"] = $("input[name='code']").val();
      tugush.API.call({
        method: "POST",
        url: "checksms",
        data: data,
        ok: function(data){
          if(data['acc']){
            tugush.API.userPatch({'sms_verification' : true}, function(){
              tugush.UI.showMessageAndGoTo("Ваш код принят. Вы будете перенаправлены.", "/agreement4");
            });
          }
          else {
            tugush.UI.showMessage("Не правильный код");
          }
        }
      });
    },
    errorPlacement: function(error, element) {
      error.appendTo('#errordiv');
    },
    ignore: [],
    rules: {
      code: {
        required: true,
      },
    },
    messages: {
      code: "Пожалуйста, введите код из смс",
    },
    onkeyup: function(element) {
      $(element).siblings('.error').remove();
      $(element).valid();
    },
    errorPlacement: function (error, element) {
      if(element.val().length == 0) {
        error.insertAfter(element);
      }
      error.click(function(event) {
        element.focus();
        error.remove();
      });
    }
  });

});
