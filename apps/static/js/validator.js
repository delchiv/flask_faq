function validator() {
    // $("#personal-account-form").validate({
    //     rules:{
    //         official_name:{
    //             required: true
    //         },
    //         official_surname:{
    //             required: true
    //         },
    //         alias_name:{
    //             required: true
    //         },
    //         alias_surname:{
    //             required: true
    //         },
    //         shortinfo:{
    //             required: true
    //         },
    //         birthday:{
    //             required: true
    //         },
    //         email:{
    //             required: true,
    //             email:true
    //         },
    //         phone:{
    //             required: true
    //         }
    //     },
    //     messages:{
    //         official_name:{
    //             required: "Обязательное поле для наполнения!"
    //         },
    //         official_surname:{
    //             required: "Обязательное поле для наполнения!"
    //         },
    //         alias_name:{
    //             required: "Обязательное поле для наполнения!"
    //         },
    //         alias_surname:{
    //             required: "Обязательное поле для наполнения!"
    //         },
    //         shortinfo:{
    //             required: "Обязательное поле для наполнения!"
    //         },
    //         birthday:{
    //             required: "Обязательное поле для наполнения!"
    //         },
    //         email:{
    //             required: "Обязательное поле для наполнения!",
    //             email:"Введите корректный email!"
    //         },
    //         phone:{
    //             required: "Обязательное поле для наполнения!"
    //         }
    //     }
    //
    // });
    $("#changePassword").validate({
        rules: {
            old_password: {
                required: true
            },
            new_password: {
                required: true
            },
            repetition_password: {
                required: true,
                equalTo: "#newpassword"
            }
        },
        messages:{
            old_password: {
                required: "Обязательное поле для наполнения!"
            },
            new_password: {
                required: "Обязательное поле для наполнения!"
            },
            repetition_password: {
                required: "Обязательное поле для наполнения!",
                equalTo: "Пароли не совпадают!"
            }
        }
    });
    $("#agreement-individ").validate({
        rules:{
            name_organization:{
                required: true
            },
            ogrn:{
                required: true,
                digits: true
            },
            kpp:{
                required: true,
                digits: true
            },

            ind_surname:{
                required: true
            },
            ind_patronymic:{
                required: true
            },
            ind_name:{
                required: true
            },
            ind_inn:{
                required: true,
                digits: true
            },
            passport_series:{
                required: true,
                digits: true
            },
            passport_number:{
                required: true,
                digits: true
            },
            issued_by:{
                required: true,
            },
            when_issued:{
                required: true
            },
            sity:{
                required: true
            },
            street:{
                required: true
            },
            home:{
                required: true
            },
            apartment:{
                required: true
            },
            index:{
                required: true,
                digits: true
            },
            bik:{
                required: true,
                digits: true
            },
            loro_account:{
                required: true,
                digits: true
            },
            current_account:{
                required: true,
                digits: true
            },
            phone:{
                required: true
            }
        },
        messages:{
            ogrn:{
                required: "Обязательное поле для наполнения!"
            },
            ogrn:{
                required: "Обязательное поле для наполнения!"
            },
            kpp:{
                required: "Обязательное поле для наполнения!"
            },

            ind_surname:{
                required: "Обязательное поле для наполнения!"
            },
            ind_patronymic:{
                required: "Обязательное поле для наполнения!"
            },
            ind_name:{
                required: "Обязательное поле для наполнения!"
            },
            ind_inn:{
                required: "Обязательное поле для наполнения!",
                digits: "Некорректно введена информация"
            },
            passport_series:{
                required: "Обязательное поле для наполнения!",
                digits: "Некорректно введена информация"
            },
            passport_number:{
                required: "Обязательное поле для наполнения!",
                digits: "Некорректно введена информация"
            },
            issued_by:{
                required: "Обязательное поле для наполнения!",
            },
            when_issued:{
                required: "Обязательное поле для наполнения!"
            },
            sity:{
                required: "Обязательное поле для наполнения!"
            },
            street:{
                required: "Обязательное поле для наполнения!"
            },
            home:{
                required: "Обязательное поле для наполнения!"
            },
            apartment:{
                required: "Обязательное поле для наполнения!"
            },
            index:{
                required: "Обязательное поле для наполнения!",
                digits: "Некорректно введена информация"
            },
            bik:{
                required: "Обязательное поле для наполнения!",
                digits: "Некорректно введена информация"
            },
            loro_account:{
                required: "Обязательное поле для наполнения!",
                digits: "Некорректно введена информация"
            },
            current_account:{
                required: "Обязательное поле для наполнения!",
                digits: "Некорректно введена информация"
            },
            phone:{
                required: "Обязательное поле для наполнения!"
            }

        }

    });
}
