function is_touch_device() {
    return !!('ontouchstart' in window) || !!('onmsgesturechange' in window);
}
var w = $(window).width();
$(window).load(function(){
    $('.site-loader').fadeOut("slow");
});
$(document).ready(function () {
    var w = $(window).width();
    $(window).resize(function () {
        w = $(window).width();
    });
    sliders();
    // validator();
    $("#hamburger").prop('checked', false);
    $("#hamburger").on('click', function () {
        $(".menu").toggleClass("fixed");
        $("#hamburger ~ nav ").toggleClass("openMenu");
        $("html").toggleClass("scrolllock");
        $(".autentification").css({'display':'none'});

    });
    if ($("#slider1").length) {
        polzunki()
    }
    ;
    $(".money").each(function () {
        $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
    });
    $('.gray-dropdown_button').on('show.bs.collapse', function () {
        $(this).css({"display": "none"});
    })
    $('.second_level_dropdown').on('click', 'li > .navbar-header', function () {
        $('.second_level_dropdown .navbar-collapse').collapse('hide');
        $(this).next().collapse('show');
    });
    //$(document).on("click.bs.dropdown.data-api", ".second_level_dropdown_button1", function () {
    //   $("#navbar_polzunki21").collapse('hide');
    //   $("#navbar_polzunki31").collapse('hide');
    //});
    //$(document).on("click.bs.dropdown.data-api", ".second_level_dropdown_button2", function () {
    //   $("#navbar_polzunki11").collapse('hide');
    //   $("#navbar_polzunki31").collapse("hide");
    //});
    //$(document).on("click.bs.dropdown.data-api", ".second_level_dropdown_button3", function () {
    //   $("#navbar_polzunki11").collapse('hide');
    //   $("#navbar_polzunki21").collapse('hide');
    //});
    //$(document).on("tap.bs.dropdown.data-api", ".second_level_dropdown_button1", function () {
    //   $("#navbar_polzunki21").collapse('hide');
    //   $("#navbar_polzunki31").collapse('hide');
    //});
    //$(document).on("tap.bs.dropdown.data-api", ".second_level_dropdown_button2", function () {
    //   $("#navbar_polzunki11").collapse('hide');
    //   $("#navbar_polzunki31").collapse("hide");
    //});
    //$(document).on("tap.bs.dropdown.data-api", ".second_level_dropdown_button3", function () {
    //   $("#navbar_polzunki11").collapse('hide');
    //   $("#navbar_polzunki21").collapse('hide');
    //});
    /////////////////////////////////////////////////////////
    /*Ограничение по длинне символов*/
    $(".completedProgram_about").each(function () {
        if ($(this).text().length > 60) {
            var text1 = ($(this).text().substring(0, 60) + '...');
            $(this).text(text1);
        }
    });
    /*Ограничение по длинне символов*/
    //////////////////////////////////////////////////////////
    /*Выдвижение подписки*/
    var subscribetext = $(".subscribe input[type='text']");
    var subscribesubmit =  $(".subscribe input[type='submit']");
    $(".subscribe").on('click', 'input:not(.open)', function (event) {
        event.stopPropagation();
        $(".progectHeaderLinks a").addClass("subscribeclose");

        setTimeout(function () {
            subscribetext.addClass("subscribeopen");
            subscribetext.css({'padding': '0 10px'});
        }, 200);
        subscribesubmit.addClass("open");
        subscribesubmit.css({'background-color': '#3f81c7'});
    });
    $("body").on('click', "*:not(.open)", function () {
        subscribetext.removeClass("subscribeopen");
        setTimeout(function () {
            $(".progectHeaderLinks a").removeClass("subscribeclose");
        }, 200);
        subscribesubmit.removeClass("open");
        subscribetext.css({'padding': '0px'});
        subscribesubmit.css({'background-color': 'transparent'});
    });
    $(".subscribe").on('click', '.open', function (event) {
        event.stopPropagation();
        if ($(this).find(".subscribeMail").val() == "") {
            subscribetext.removeClass("subscribeopen");
            setTimeout(function () {
                $(".progectHeaderLinks a").removeClass("subscribeclose");
            }, 200);
            subscribesubmit.removeClass("open");
            subscribetext.css({'padding': '0px'});
            subscribesubmit.css({'background-color': 'transparent'});
        }
        else {
            subscribetext.removeClass("subscribeopen");
            setTimeout(function () {
                $(".progectHeaderLinks a").removeClass("subscribeclose");
            }, 200);
            subscribesubmit.removeClass("open");
            subscribetext.css({'padding': '0px'});
            subscribesubmit.css({'background-color': 'transparent'});
            /*data = {
             'subscribeMail': $(".subscribeMail").val(),
             };
             $.ajax({
             type: 'POST',
             url: '',
             dataType: 'json',
             data: data,
             success: function(json) {

             }
             });*/
        }
    });
    /*Выдвижение подписки*/

    //////////////////////////////////////////////////

    /*Переключение на странице проекта на мобиле*/
    $(".progectContentHeader").on("click",function(){
        $(".progectContent").each(function(){
            if($(this).css('display') == 'block'){
                $(this).slideToggle();
            }
        });
        if($(this).next(".progectContent").css('display') == 'none'){
            $(this).next(".progectContent").slideToggle();
        }
    });
    /*Переключение на странице проекта на мобиле*/
    /*Переключение на странице проекта на десктопе*/
    $(".NavigationLinksgroup > div").on("click", function() {
        if ($(this).hasClass('scrollTo')){
            var id  = $(this).attr('data-type');
            id = '#' + id;
            var top = $(id).offset().top;
            $('body,html').animate({scrollTop: top-60}, 700);
            return false;
        }
        var number = $(this).index();
        var index = (number + 1) * 2;
        $(".progectTabs .progectContent").css({'display': 'none'});
        $(".progectTabs .progectContent:nth-child(" + index + ")").fadeIn(500);

        $(".NavigationLinksgroup>div").each(function() {
            $(this).removeClass("active_point");
        });
        $(this).addClass("active_point");
        if ($(this).is("[data-hide_rightpart]")) {
          $(".progectTabs").addClass("full-width");
          $(".overview_rightpart").hide();
        } else {
          $(".progectTabs").removeClass("full-width");
          $(".overview_rightpart").show();
        }
    });
    /*Переключение на странице проекта на десктопе*/

    //////////////////////////////////////////////////////////

    /*Переключение авторизации и регистрации*/

    // $(".autentification-button").on('click',function (e) {
    //     e.preventDefault();
    //     $("#hamburger").prop('checked', false);
    //     $("#hamburger ~ nav ").removeClass("openMenu");
    //     if (w < 1000) {
    //         $(".menu").toggleClass("fixed");
    //         $(".autentification").slideToggle("fast");
    //         $("body").css({'overflow':'hidden'});
    //         $(".autentification").css({'display':'flex'});
    //     }
    //     else if (w > 1000) {
    //         $(".autentification").fadeIn("slow");
    //         $(".autentification").css({'display':'flex'});
    //     }
    //
    // });
    $(".close-autentification").on('click',function () {
        if (w < 1000) {
            $(".menu").toggleClass("fixed");
            $(".autentification").slideToggle("fast");
            $("body").css({'overflow':'visible'});
        }
        else if (w > 1000) {
            $(".autentification").fadeOut("slow");
            $("body").css({'overflow':'visible'});
        }
    });

    $(".login_in_site .toggle_button").on('click',function(){
        $(".registration_in_site .toggle_content").css({'display':'none'});
        $(this).siblings(".toggle_content").slideToggle("fast");
    });
    $(".registration_in_site .toggle_button").on('click',function(){
        $(".login_in_site .toggle_content").css({'display':'none'});
        $(this).siblings(".toggle_content").slideToggle("fast");
    });

    /*Переключение авторизации и регистрации*/
    //////////////////////////////////////////////////////////
    /*Добавление новой социальной сети в личном кабинете*/
    $(".moreNetvorks").on("click",function () {
        $(this).siblings("input").after("<input class='text moreinput' placeholder='http://' type='text' />");
    });
    //////////////////////////////////////////////
    /*Календарь*/
    $('#datetimepicker1').datetimepicker( {pickTime: false,language: 'ru'});
    /*Календарь*/
    ////////////////////////////////////////////////
    /*Телефон*/
    jQuery(function($){
        $(".phone").mask("+7(999) 999-99-99");
    });
    /*Телефон*/
    //////////////////////////////////////////
    /*Добавление новой социальной сети в личном кабинете*/
    //////////////////////////////////////////////////////////
    /*Переключение договора*/
    var agreementDiv1 = $(".agreement .first-element .toggle-block");
    var agreementDiv2 = $(".agreement .second-element .toggle-block");
    var agreementDiv3 = $(".agreement .third-element .choise-container");
    var agreementDiv4 = $(".agreement .fourth-element .choise-container");
    agreementDiv3.css({'display':'block'});
    agreementDiv1.addClass("agreement-active");
    agreementDiv1.on('click',function () {
        agreementDiv2.removeClass("agreement-active");
        if (w < 765) {
            $(this).toggleClass("agreement-active");
            agreementDiv4.slideUp("fast");
            agreementDiv3.slideToggle("fast");
        }
        else{
            $(this).addClass("agreement-active");
            agreementDiv4.css({'display':'none'});
            agreementDiv3.fadeIn("fast");
        }
    });
    agreementDiv2.on('click',function () {
        agreementDiv1.removeClass("agreement-active");
        if (w < 765) {
            $(this).toggleClass("agreement-active");
            agreementDiv3.slideUp("fast");
            agreementDiv4.slideToggle("fast");
        }
        else{
            $(this).addClass("agreement-active");
            agreementDiv3.css({'display':'none'});
            agreementDiv4.fadeIn("fast");
        }
    });
    /*Переключение договора*/

    /*Выбор создание компании*/
    /*Переключение договора*/

    $(".create-company-toggle-container .reflow:nth-child(odd)").on("click", function(){
      $(".create-company-toggle-container .reflow:nth-child(even)").addClass("hidden");
      $(".create-company-toggle-container .reflow:nth-child(odd) .toggle-block").removeClass("create-company-active");
      $(this).next().removeClass("hidden");
      $(this).children(".toggle-block").addClass("create-company-active");
    });
    // var companyDiv1 = $(".create-company .reflow:nth-child(1) .toggle-block");
    // var companyDiv2 = $(".create-company .reflow:nth-child(4) .toggle-block");
    // var companyDiv3 = $(".create-company .reflow:nth-child(2) .toggle-block");
    // var companyDiv4 = $(".create-company .reflow:nth-child(5) .choise-container");
    // var companyDiv5 = $(".create-company .reflow:nth-child(3) .choise-container");
    // var companyDiv6 = $(".create-company .reflow:nth-child(6) .choise-container");
    // companyDiv4.css({'display':'block'});
    // companyDiv1.addClass("create-company-active");
    // companyDiv1.on('click',function () {
    //     companyDiv2.removeClass("create-company-active");
    //     companyDiv3.removeClass("create-company-active");
    //     if (w < 765) {
    //         $(this).toggleClass("create-company-active");
    //         companyDiv5.slideUp("fast");
    //         companyDiv5.find("input").each(function () {
    //             $(this).prop("checked", false);
    //         });
    //         companyDiv6.slideUp("fast");
    //         companyDiv6.find("input").each(function () {
    //             $(this).prop("checked", false);
    //         });
    //         companyDiv4.slideToggle("fast");
    //     }
    //     else{
    //         $(this).addClass("create-company-active");
    //         companyDiv5.css({'display':'none'});
    //         companyDiv5.find("input").each(function () {
    //             $(this).prop("checked", false);
    //         });
    //         companyDiv6.css({'display':'none'});
    //         companyDiv6.find("input").each(function () {
    //             $(this).prop("checked", false);
    //         });
    //         companyDiv4.fadeIn("fast");
    //     }
    // });
    // companyDiv2.on('click',function () {
    //     companyDiv1.removeClass("create-company-active");
    //     companyDiv3.removeClass("create-company-active");
    //     if (w < 765) {
    //         $(this).toggleClass("create-company-active");
    //         companyDiv4.slideUp("fast");
    //         companyDiv4.find("input").each(function () {
    //             $(this).prop("checked", false);
    //         });
    //         companyDiv6.slideUp("fast");
    //         companyDiv6.find("input").each(function () {
    //             $(this).prop("checked", false);
    //         });
    //         companyDiv5.slideToggle("fast");
    //     }
    //     else{
    //         $(this).addClass("create-company-active");
    //         companyDiv4.css({'display':'none'});
    //         companyDiv4.find("input").each(function () {
    //             $(this).prop("checked", false);
    //         });
    //         companyDiv6.css({'display':'none'});
    //         companyDiv6.find("input").each(function () {
    //             $(this).prop("checked", false);
    //         });
    //         companyDiv5.fadeIn("fast");
    //     }
    // });
    // companyDiv3.on('click',function () {
    //     companyDiv1.removeClass("create-company-active");
    //     companyDiv2.removeClass("create-company-active");
    //     if (w < 765) {
    //         $(this).toggleClass("create-company-active");
    //         companyDiv4.slideUp("fast");
    //         companyDiv4.find("input").each(function () {
    //             $(this).prop("checked", false);
    //         });
    //         companyDiv5.slideUp("fast");
    //         companyDiv5.find("input").each(function () {
    //             $(this).prop("checked", false);
    //         });
    //         companyDiv6.slideToggle("fast");
    //     }
    //     else{
    //         $(this).addClass("create-company-active");
    //         companyDiv4.css({'display':'none'});
    //         companyDiv4.find("input").each(function () {
    //             $(this).prop("checked", false);
    //         });
    //         companyDiv5.css({'display':'none'});
    //         companyDiv5.find("input").each(function () {
    //             $(this).prop("checked", false);
    //         });
    //         companyDiv6.fadeIn("fast");
    //     }
    // });
    /*Переключение договора*/
    /*Выбор создание компании*/

    //////////////////////////////////////////////
    /*Появление формы вопроса на странице проекта*/
    $(".showQuestionFormData").on('click',function () {
        $(this).slideUp("fast");
        $(".formPushQuestion").slideDown("fast");
    });
    /*Появление формы вопроса на странице проекта*/
    //////////////////////////////////////////////


    //////////////////////////////////////////////
    /*Договор верификации уточнение адреса*/
    $(".more-addres").prop("checked", false);
    $(".more-addres").on('click',function () {
        $(".real-addres").slideToggle("normal");
        $(".real-addres input").each(function () {
            $(this).val("") ;
        })
    });
    /*Договор верификации уточнение адреса*/
    ///////////////////////////////////////////////

    /*Текстовый редактор*/
    tinymce.init({
        selector: '.tiny-text',
        height: 300,
        language: 'ru',
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        automatic_uploads: true,
        file_browser_callback: function(field_name, url, type, win) {
          if(type=='image') $('#my_form input').click();
        },
        setup: function (editor) {
          editor.on('change', function () {
            editor.save();
          });
        }
    });
    /*Текстовый редактор*/
    //////////////////////////////////////////////

    if (is_touch_device()) {
        if ($('.ui-slider-handle').length) {
            $(this).draggable();
        }
    }

    //страница инвестиций проекта
    $('input[name="agree_invest"]').click(function(event) {
      $('.project_invest .progectTabs section.step:nth-child(3)').toggleClass('active');
    });
    $('.project_invest .progectTabs section.step .methods_pay a').click(function(event) {
      $('.project_invest .progectTabs section.step .methods_pay a').removeClass('active');
      $(this).toggleClass('active');
    });

    if($('input[name="role"]').length) {
      $('input[name="role"]').click(function(event) {
        localStorage.setItem("next_page", $(this).data("next-page"));
      });
    }

    // $('.registration_in_site .network_login a').click(function(event) {
    //   if($('.registration_in_site input[name="role"]').valid()){
    //     return true;
    //   }
    //   return false;
    // });
    calc_percent();
    $('.zero-percent .input-wrap input').on('keyup input', function(){
        calc_percent();
    })


    $('.create-company input[name="actions"]').mask("9?99999999");
    $('.create-company input[name="investment_target"],input[name="start_capital"],input[name="estimated_profit"],input[name="investment_min"]').mask("9?9999999999999999999999999999999");
    $('.create-company input[name="website"]').mask("?http://?***********************************************************");
    
    //Маска для паспортных данных
    $('[name=passport_series]').mask('99 99 999999', {placeholder: '_'});
    $('[name=passport_number]').mask('999-999', {placeholder: '_'});		//Код подразделения
    //Маска на ИНН
    $('[name=ind_inn]').mask('9999999999?99', {placeholder: '_'});
});

// SHARE BUTTON


Share = {
    vkontakte: function() {
        url  = 'http://vkontakte.ru/share.php?';
        url += 'url='          + window.location;
        Share.popup(url);
    },
    facebook: function() {
        url  = 'http://www.facebook.com/sharer.php?s=100';
        url += '&p[url]='       + window.location;
        Share.popup(url);
    },
    twitter: function() {
        url  = 'http://twitter.com/share?';
        url += '&url='      + window.location;
        Share.popup(url);
    },
    gplus: function() {
        url  = 'https://plus.google.com/share?';
        url += '&url='      + window.location;
        Share.popup(url);
    },
    in: function() {
        url  = 'https://www.linkedin.com/shareArticle?mini=true';
        url += '&url='      + window.location;
        Share.popup(url);
    },

    popup: function(url) {
        window.open(url,'','toolbar=0,status=0,width=626,height=436');
    }
};

function calc_percent(){
    var percent = parseInt($('.zero-percent .text1 .right span').html())/100 * $('.zero-percent .input-wrap input').val();
    $('.zero-percent .text2 .right').html(percent);
}
