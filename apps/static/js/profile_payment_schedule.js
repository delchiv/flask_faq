$(document).ready(function() {
  $(".project-schedule").addClass("hidden");
  $(".project-schedule").eq(0).removeClass("hidden");
  $("[data-contents='payment_schedules'] select").on("change", function() {
    var toshow = $(this).val();
      $(".project-schedule").addClass("hidden");
      $(".project-schedule[data-id='"+toshow+"']").removeClass("hidden");
  });
});
