$(document).ready(function(){
  //добавление инвестиции
  $('#paymentFormSample button').click(function(event) {
    event.preventDefault();
    var err1 = 0,
    err2 = 0,
    error = 0;
    var field = new Array("price", "rate");
    $(".project_invest").find(":input").each(function() {
				for(var i=0;i<field.length;i++){
					if($(this).attr("name")==field[i]){
            $(this).removeClass('error');
            $(this).next('label').remove();
						if(!$(this).val()){
							$(this).addClass('error');
              $(this).after('<label class="error">Заполните данное поле</label>')
							error=1;
						}
						else{
              $(this).removeClass('error');
              $(this).next('label').remove();
						}
					}
				}
		   })
    if(error == 1){
      var target = ($('input[name="price"]').offset().top)-95;
		  $('html, body').animate({scrollTop:target}, 800);

    }
    else {
      createCryptogram();
    }
  });

  $('input[name="price"],input[name="rate"]').on('keyup', function(event) {
    var thh = $(this);
    if(thh.val()){
      thh.removeClass('error');
      thh.next('label').remove();
    }
  });
  $('input[name="price"]').mask("99999?9", { placeholder: " " });
  $('input[name="rate"]').mask("9?9", { placeholder: " " });

  $('input[data-cp="cardNumber"]').mask("9999 9999 9999 9?999", { placeholder: "4925 0000 0000 0087" });
  $('input[data-cp="cvv"]').mask("999", { placeholder: " " });
  $('input[data-cp="expDateMonth"]').mask("99", { placeholder: " " });
  $('input[data-cp="expDateYear"]').mask("99", { placeholder: " " });
  $('input[name="duration"]').mask("9?99", { placeholder: " " });


  $('input[data-cp="cardNumber"],input[data-cp="expDateMonth"],input[data-cp="cvv"],input[data-cp="expDateYear"],input[data-cp="name"]').on('keyup', function(event) {
    $(this).removeClass('error').next('label').remove();
  });

  if($('.3d_sec_special').length){
    if($('.3d_sec_special').data('res') == 1){
      tugush.UI.showMessageAndGoTo("Ваша инвестиция принята, результат будет отправлен на почту", "/");
    }
    else {
      tugush.UI.showMessageAndGoTo("Возникла ошибка, попробуйте в другой раз", "/");
    }
  }

});
var createCryptogram = function () {
    var user_name = $('input[data-cp="name"]').val();
    var result = checkout.createCryptogramPacket();
    $('#paymentFormSample input').removeClass('error');
    $('#paymentFormSample input').next('label').remove();
    if (result.success) {
      console.log(result);
        $('#paymentFormSample input').removeClass('error');
        $('#paymentFormSample input').next('label').remove();
        // сформирована криптограмма
        // console.log(result.packet);
        var data = {};
        data['cid'] = $('input[name="cid"]').val();
        data['price'] = $('input[name="price"]').val();
        data['rate'] = $('input[name="rate"]').val();
        data['duration'] = $('input[name="duration"]').val();
        data['crg'] = result.packet;
        data["card_name"] = user_name;
        tugush.API.call({
            method: "POST",
            url: "sendinvest",
            data: data,
            ok: function(data){

              invest_id = data['id'];
              if(data["model"]){
                var model = data["model"];
                var form = $('form[name="downloadForm"]');
                form.attr('action',model['AcsUrl']);
                form.find('input[name="PaReq"]').val(model['PaReq']);
                form.find('input[name="MD"]').val(model['TransactionId']);
                form.find('input[name="TermUrl"]').val(data['url']);
                form.submit();
              }
              if(data["succ"]){
                tugush.UI.showMessageAndGoTo("Ваша инвестиция принята, результат будет отправлен на почту", "/");
              }
              if(data["error"]){
                tugush.UI.showMessageAndGoTo("Возникла ошибка, попробуйте в другой раз", "/");
              }
            }
          });
    }
    else {
        // найдены ошибки в ведённых данных, объект `result.messages` формата:
        // { name: "В имени держателя карты слишком много символов", cardNumber: "Неправильный номер карты" }
        // где `name`, `cardNumber` соответствуют значениям атрибутов `<input ... data-cp="cardNumber">`
        var target = ($('input[data-cp="cardNumber"]').offset().top)-95;
  		  $('html, body').animate({scrollTop:target}, 800);
       for (var msgName in result.messages) {
          //  alert(result.messages[msgName]);
          var inp = $('input[data-cp='+msgName+']');
          if(inp.next('label')) {
            inp.next('label').remove();
          }
          inp.removeClass('error');
          inp.addClass('error');
          inp.after('<label class="error">'+result.messages[msgName]+'</label>');

       }
    }
};
$(function () {
    /* Создание checkout */
    checkout = new cp.Checkout(
    // public id из личного кабинета
    "pk_c8b5d92420cd72dc3cbf07a3d5c15",
    // тег, содержащий поля данными карты
    document.getElementById("paymentFormSample"));
});
