if (typeof admin === 'undefined') {
  var admin = {};
}

admin.API = {
  call: function(params){
    $.ajax({
      method: params.method,
      url: "/api/admin/"+params.url,
      data: JSON.stringify(params.data),
      contentType: "application/json; charset=utf-8",
      dataType: "json"
    }).done(function(data){
      params.ok(data);
    }).fail(function(XHR_obj, status, error){
      if ("fail" in params) {
      }
      params.fail(XHR_obj, status, error);
    });
  },
  questionPost: function(data, ok){
    var self = this;
    self.call({
      method: "POST",
      url: "questionnaire/question",
      data: data,
      ok: ok
    });
  },
  questionGet: function(question_id, ok){
    var self = this;
    self.call({
      method: "GET",
      url: "questionnaire/question/"+question_id,
      data: {},
      ok: ok
    });
  },
  questionPatch: function(question_id, data, ok){
    var self = this;
    self.call({
      method: "PATCH",
      url: "questionnaire/question/"+question_id,
      data: data,
      ok: ok
    });
  },
  questionDelete: function(question_id, ok){
    var self = this;
    self.call({
      method: "DELETE",
      url: "questionnaire/question/"+question_id,
      data: {},
      ok: ok
    });
  }
  // cardsGet: function(ok){
  //   var self = this;
  //   self.call({
  //     method: "GET",
  //     url: "me/cards",
  //     data: {},
  //     ok: ok
  //   });
  // },
  // cardsDelete: function(card_id, ok){
  //   var self = this;
  //   self.call({
  //     method: "DELETE",
  //     url: "me/cards/"+card_id,
  //     data: {},
  //     ok: ok
  //   });
  // }
};
