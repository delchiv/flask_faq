function normalPOlzunik(number) {

    var range = {
        'min': parseInt($("#minCost"+number+"").val()),
        'max': parseInt($("#maxCost"+number+"").val())
    }

    $("#slider"+number+"").slider({
        min: range.min,
        max: range.max,
        values: [range.min, range.max],
        range: true,
        stop: function (event, ui) {
            $("input#minCost"+number+"").val(ui.values[0]);
            $("input#maxCost"+number+"").val(ui.values[1]);

        },
        slide: function (event, ui) {
            $("input#minCost"+number+"").val(ui.values[0]);
            $("input#maxCost"+number+"").val(ui.values[1]);
        }
    });

    $("input#minCost"+number+"").change(function () {
        var value1 = $("input#minCost"+number+"").val();
        var value2 = $("input#maxCost"+number+"").val();

        if (parseInt(value1) > parseInt(value2)) {
            value1 = value2;
            $("input#minCost"+number+"").val(value1);
        }
        $("#slider"+number+"").slider("values", 0, value1);
    });

    $("input#maxCost"+number+"").change(function () {
        var value1 = $("input#minCost"+number+"").val();
        var value2 = $("input#maxCost"+number+"").val();

        if (value2 > range.max) {
            value2 = range.max;
            $("input#maxCost"+number+"").val(range.max)
        }

        if (parseInt(value1) > parseInt(value2)) {
            value2 = value1;
            $("input#maxCost"+number+"").val(value2);
        }
        $("#slider"+number+"").slider("values", 1, value2);
    });
}

function procentPOlzunki(number) {

    var range = {
        'min': parseInt($("#minCost"+number+"").val()),
        'max': parseInt($("#maxCost"+number+"").val())
    };
    var minValue = $("input#minCost"+number+"");
    $("#slider"+number+"").slider({
        min: range.min,
        max: range.max,
        values: [range.min, range.max],
        range: true,
        stop: function (event, ui) {
            $("input#minCost"+number+"").val(ui.values[0] + "%");
            $("input#maxCost"+number+"").val(ui.values[1] + "%");

        },
        slide: function (event, ui) {
            $("input#maxCost"+number+"").val(ui.values[1] + "%");
            $("input#minCost"+number+"").val(ui.values[0] + "%");
        }
    });
    $("input#minCost"+number+"").val($("#slider"+number+"").slider("values", 0) + "%");
    $("input#maxCost"+number+"").val($("#slider"+number+"").slider("values", 1) + "%");

    $("input#minCost"+number+"").change(function () {
        var value1 = $("input#minCost"+number+"").val();
        var value2 = $("input#maxCost"+number+"").val();

        if (parseInt(value1) > parseInt(value2)) {
            value1 = value2;
            $("input#minCost"+number+"").val(value1);
        }
        $("#slider"+number+"").slider("values", 0, value1);
    });

    $("input#maxCost"+number+"").change(function () {
        var value1 = $("input#minCost"+number+"").val();
        var value2 = $("input#maxCost"+number+"").val();

        if (value2 > range.max) {
            value2 = range.max;
            $("input#maxCost1").val(range.max)
        }

        if (parseInt(value1) > parseInt(value2)) {
            value2 = value1;
            $("input#maxCost"+number+"").val(value2);
        }
        $("#slider"+number+"").slider("values", 1, value2);
    });
}

function polzunki() {
    /*Минимальный размер инвестиций*/
        normalPOlzunik(0);
    /*Минимальный размер инвестиций*/
    /////////////////////////////////////////////////////////
    /* Ползунок Прогнозируемая доходность */
        procentPOlzunki(1);
    /* Ползунок Прогнозируемая доходность  */
    ////////////////////////////////////////////////////////
    /* Ползунок Передпологаемый пакет акций */
        procentPOlzunki(2);
    /* Ползунок Передпологаемый пакет акций  */
    ////////////////////////////////////////////////////////
    /* Ползунок уже проинвестировано */
        procentPOlzunki(3);
    /* Ползунок Минимальный размер инвестиций */
    ///////////////////////////////////////////////////////
    /* Ползунок Срок займа */
        normalPOlzunik(4);
    /* Ползунок Срок займа */
    ///////////////////////////////////////////////////////
    /*Ползунок процентная ставка*/
        procentPOlzunki(5);
    /*Ползунок процентная ставка*/
    //////////////////////////////////////////////////////
    /*Ползунок Доход от ренты*/
        procentPOlzunki(6);
    /*Ползунок Доход от ренты*/
    //////////////////////////////////////////////////////
    /*Ползунок Повышение стоимости*/
        procentPOlzunki(7);
    /*Ползунок Повышение стоимости*/
    //////////////////////////////////////////////////////
    /*Ползунок Окупаемость (месяцев)*/
        normalPOlzunik(8);
    /*Ползунок Окупаемость (месяцев)*/
    //////////////////////////////////////////////////////
}
