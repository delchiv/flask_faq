var subjects = [];
var cities = [];
var cits = [];
var sou = [];
var next = {};

function printResults(predictions, status) {
    if (status != google.maps.places.PlacesServiceStatus.OK) {
        console.log(status);
        return;
    } else {
        cits.push({
            "name": next.name,
            "place_id": predictions[0].place_id
        })
        if (next = sou.pop()) {
            setTimeout(function(){
                searchGoogle(next.name);
            }, 500);
        } else {
            console.log(JSON.stringify(cits));
        }
    }
}
function searchGoogle(search) {
    var service = new google.maps.places.AutocompleteService();
    if (search) {
        service.getQueryPredictions({ input: search }, printResults);
    }
}
function fillCities(predictions, status) {
    if (status != google.maps.places.PlacesServiceStatus.OK) {
        return;
    } else {
        output = [];
        predictions.forEach(function(prediction) {
            if ('types' in prediction) {
                if (prediction.types[0] == 'locality') {
                    var region = "";
                    if (1 in prediction.terms) {
                        region = region + prediction.terms[1].value;
                    }
                    if (2 in prediction.terms) {
                        region = region + ', ' + prediction.terms[2].value;
                    }

                    output.push({
                        "name":prediction.terms[0].value,
                        "region":region});
                }
            }
        });
        if (output.length > 0) {
            fillWithMatches($('.searchblock .city input'),output, $('.searchblock .city .dropdown ul'), true);
        }
    }
}
function fillAddresses(predictions, status) {
    if (status != google.maps.places.PlacesServiceStatus.OK) {
        console.log(status);
        return;
    } else {
        output = [];
        predictions.forEach(function(prediction) {
            if ('types' in prediction) {
                if (prediction.types[0] == 'route') {
                    output.push({
                        "name":prediction.terms[0].value
                    });
                }
            }
        });
        if (output.length > 0) {
            if ($('.searchblock .address .dropdown ul').length > 0){
                fillWithMatches($('.searchblock .address input'),output, $('.searchblock .address .dropdown ul'), true);
            } else {
                fillWithMatches($('.inputblock.address input'),output, $('.inputblock.address .dropdown ul'), true);
            }
        }
    }
}
function getCities(search) {
    var service = new google.maps.places.AutocompleteService();
    if (search) {
        service.getQueryPredictions({ input: search }, fillCities);
    }
}
function getAddresses(search) {
    var service = new google.maps.places.AutocompleteService();
    if (search) {
        service.getQueryPredictions({ input: search }, fillAddresses);
    }
}
function resetListeners(){
    $(document).off('keydown');
}
function scrollToActiveLi(ul){
    var targetTop = ul.find('li.active').offset().top - ul.offset().top;
    if (targetTop > ul.height() - 1) {
        ul.scrollTop(targetTop+ul.find('li.active').outerHeight()+ul.scrollTop()-ul.height());
    }
    if (targetTop < 0) {
        ul.scrollTop(targetTop+ul.scrollTop());
    }
}
function fillWithMatches(input,records,container,fillany) {
    resetListeners();
    container.scrollTop(0);
    var search = input.val().toLowerCase();
    if (search.length < 1) {
        container.html('');
        for (var rec in records) {
            if (records[rec].image) {
                $('<li></li>').html(records[rec].name).attr('data-bg',records[rec].image).appendTo(container);
            } else if (records[rec].value) {
                $('<li></li>').html(records[rec].value).attr('data-sourceid',records[rec].id).appendTo(container);
            } else {
                $('<li></li>').html(records[rec].name).appendTo(container);
            }
        }
    } else {
        var result = [];
        for (var rec in records) {
            if (fillany || records[rec].name.indexOf(search) > -1) {
                var hit = {};
                hit[0] = records[rec].name;
                hit[1] = records[rec].image;
                if (records[rec].id) {
                    hit[2] = records[rec].id;
                }
                if ('region' in records[rec]) {
                    hit['region'] = records[rec].region;
                } else {
                    hit['region'] = '';
                }
                result.push(hit);
            }
        }
        if (!fillany) {
            result.sort(function(a, b){
                return a[0].toLowerCase().indexOf(search) - b[0].indexOf(search)
            });
        }
        container.html('');
        for (var res in result) {
            if (result[res]['region']) { // Google Autocomplete results
                $('<li></li>').html(result[res][0].toLowerCase().replace(search,'<span class="hit">'+search+'</span>')+', <span class="extra">'+result[res]['region']+'</span>').appendTo(container);
            } else { // Subjects with backgrounds
                var li = $('<li></li>').attr('data-bg',result[res][1]).html(result[res][0].toLowerCase().replace(search,'<span class="hit">'+search+'</span>'));
                if (2 in result[res]) {
                    li.attr('data-sourceid',result[res][2]);
                }
                li.appendTo(container);
            }
        }
    }
    container.find('li').eq(0).addClass('active');
    if (container.offset().top+container.outerHeight()-$(document).scrollTop() > $(window).height()) {
        $('html, body').animate({
            scrollTop: $('html').scrollTop()+container.offset().top+container.outerHeight()-$(document).scrollTop()-$(window).height()+16
        }, 500);
    }
    $(document).keydown(function(e) {
        switch(e.which) {
            case 13: // enter
            case 39: // right
                container.parents('.inputblock').find('input').val(container.find('li.active').text());
                container.parents('.dropdown').addClass('hidden');
                resetListeners();
                break;

            case 9: // tab
                container.parents('.inputblock').find('input').val(container.find('li.active').text());
                resetListeners();
                break;

            case 38: // up
                if (container.find('li.active').is(':first-child')) {
                    container.find('li.active').removeClass('active');
                    container.find('li:last-child').addClass('active');
                } else {
                    container.find('li.active').removeClass('active').prev().addClass('active');
                }
                scrollToActiveLi(container);
                updateSubjectImage();
                break;

            case 40: // down
                if (container.find('li.active').is(':last-child')) {
                    container.find('li.active').removeClass('active');
                    container.find('li:first-child').addClass('active');
                } else {
                    container.find('li.active').removeClass('active').next().addClass('active');
                }
                scrollToActiveLi(container);
                updateSubjectImage();
                break;

            default: return; // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
    });
};
function updateSubjectImage(){
    var activeSubjects = $('.searchblock .subject .dropdown ul li.active');
    if (activeSubjects.length > 0) {
        introSlider.stop();
        if (activeSubjects.eq(0).data('bg')) {
            introSlider.updateRotator(activeSubjects.eq(0).data('bg'));
        } else {
            introSlider.start();
        }
    } else {
        introSlider.start();
    }
}
$(document).ready(function() {
    $.getJSON('/api/subjects.json').done(function(data){
        subjects = data;
    });
    $.getJSON('/api/performer/options').done(function(data){
        cities = data["cities"];
        for (var cit in cities) {
            cities[cit]["name"] = cities[cit]["value"].toLowerCase();
        }
    });
    $('.searchblock .subject .dropdown').on('click','li',function(){
        updateSubjectImage();
    });
    $('.searchblock input, .inputblock input').on('focus input', function(){
        $(this).siblings('.dropdown').removeClass('hidden');
    });
    $('.searchblock .subject input').on('focus input', function(){
        fillWithMatches($(this),subjects, $('.searchblock .subject .dropdown ul'), false);
        updateSubjectImage();
    });
    $('.inputblock.city input').on('focus input', function(){
        fillWithMatches($(this),cities, $('.inputblock.city .dropdown ul'), false);
    });

    $('.searchblock .city input').on('focus input', function(){
        getCities($(this).val());
    });
    $('.searchblock .address input').on('focus input', function(){
        getAddresses($('.searchblock .city input').val()+', '+$(this).val());
    });
    $('.inputblock.address input').on('focus input', function(){
        getAddresses($('.inputblock.city input').val()+', '+$(this).val());
        $(this).siblings('.dropdown').removeClass('hidden');
    });
    $('.searchblock input').on('blur', function(){
        var self = $(this);
        setTimeout(function(){
            self.siblings('.dropdown').addClass('hidden');
        },200);
        resetListeners();
    });
    $('.gotosearch').on('click', function(){
        var url = new URI(location.href);
        url.path('/search.html');
        url.search({
            subject: $('.subject input').val(),
            city: $('.city input').val()
        });
        location = url.href();
    });
    var url = new URI(location.href);
    var queries = url.search(true);
    for (var key in queries) {
        $('.inputblock.'+key+' input').val(queries[key]);
    }
});
