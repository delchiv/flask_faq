var notice_socket;
var notice_models;
var notice_templates;

function getNoticeTemplate(template_name) {
  return notice_templates.filter(".notice[data-template='" + template_name + "']");
}

function fillTemplate(template, model, data) {
  for (var key in model) {
    if (!(model[key].constructor === Array)) {
      fillTemplate(template, model[key], data);
    } else {
      var dummy = "[" + model[key][0] + "][" + model[key][1] + "]";
      var value = data[model[key][0]][model[key][1]];
      template.html(template.html().split(dummy).join(value));
    }
  }
}

$(document).ready(function() {

  $.get("/utils/notice_models", function(data) {
    notice_models = data;
  });

  $.get("/utils/notice_templates", function(data) {
    notice_templates = $($.parseHTML(data));
  });

  notice_socket = io.connect('http://' + document.domain + ':' + location.port + "/notices");

  notice_socket.on("notice", function(message) {
    var template = getNoticeTemplate(message.template);
    var model = notice_models[message.template];
    var data = message.data;
    var created_at = message.created_at;
    var badge = $(".userTool-notices .badge");

    fillTemplate(template, model, data);
    template.html(template.html().split("[created_at]").join(created_at));

    // template.html(template.html().replace("[name]", "name_replaced"));
    template.prependTo(".userTool-notices-list");
    badge.removeClass("hidden").html(parseInt(badge.html()) + 1);
  });

});
