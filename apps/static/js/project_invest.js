$(document).ready(function() {
  $("input[name='price'], input[name='rate'], input[name='duration']").on("change input keypress", function() {
    var link = $(".project_invest .chart a");
    var link_format = link.data("link_format");
    var price = $("input[name='price']").val().trim();
    var rate = $("input[name='rate']").val().trim();
    var duration = $("input[name='duration']").val().trim();
    link.attr("href", link_format
      .replace("<summ>", price)
      .replace("<percents>", rate)
      .replace("<duration>", duration)
    );
    link.toggleClass("disabled", !(price && rate && duration));
  });
});
