$(document).ready(function() {
  $("[data-action='investmentApprove'], [data-action='investmentReject']").on("click", function() {
    var self = $(this);
    var investment_id = self.data("id");
    var action = self.data("action");
    function hideThis() {
      self.parents("tr").find("td:not(:last-child)").fadeOut();
      self.parents("tr").find("td:last-child").fadeOut(function() {
        self.parents("tr").remove();
      });
    }
    tugush.API[action](investment_id, hideThis);
  });

  // Schedule pagination
  var ROWS_ON_PAGE = 5;
  $('[data-id="payment_schedule"] tr').each(function(idx) {
    if (idx > ROWS_ON_PAGE) {
      $(this).addClass("hidden");
    }
  });
  if ($('[data-id="payment_schedule"] tr').length > ROWS_ON_PAGE) {
    $('[data-id="payment_schedule"] .navigation').removeClass('hidden');
    var items_count = $('[data-id="payment_schedule"] tr').length;
    var pages = Math.ceil(items_count/ROWS_ON_PAGE);
    // if (items_count % ROWS_ON_PAGE > 0) { pages++; }
    while (pages) {
      var link = $("<a>").attr("href", "#").text(pages);
      $('[data-id="payment_schedule"] .navigation .pages').prepend(link);
      pages--;
    }

    $('[data-id="payment_schedule"] .navigation .pages a').on("click", function(e) {
      e.preventDefault();
      var page = parseInt($(this).text()) - 1;
      var start_row = page * ROWS_ON_PAGE;
      var end_row = start_row + ROWS_ON_PAGE;
      $('[data-id="payment_schedule"] tr:not(.first)').each(function(idx) {
        $(this).toggleClass("hidden", !(start_row <= idx && idx < end_row));
      });
    });
  }
});
