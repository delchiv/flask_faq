if (typeof tugush === 'undefined') {
  var tugush = {};
}

tugush.UI = {
  showMessage: function(message, callback){
    $(".overlay_message").html(message);
    var len = $(".overlay_message").text().length;
    $(".overlay_message").fadeIn().css("display", "flex");
    setTimeout(function(){
      $(".overlay_message").fadeOut(function(){
        if (callback) {
          callback();
        }
      });
    }, len*80+350);
  },
  showMessageAndGoTo: function(message, url){
    $(".overlay_message").html(message);
    var len = $(".overlay_message").text().length;
    $(".overlay_message").fadeIn().css("display", "flex");
    setTimeout(function(){
      $(".overlay_white").fadeIn(function(){
        location.href = url;
      });
    }, len*80+350);
  },
  goToTab: function(toshow){
    $("[data-id='"+toshow+"']").addClass("active").siblings("[data-id]").removeClass("active");
    if ($("[data-toshow='"+toshow+"']").hasClass("button")) {
      $("[data-toshow='"+toshow+"']").addClass("blue").removeClass("void gray").siblings("[data-toshow]").addClass("void gray").removeClass("blue");
    } else {
      $("[data-toshow='"+toshow+"']").addClass("active").siblings("[data-toshow]").removeClass("active");
      location.hash = "#" + toshow;
      if ($(window).width() < 992) {
        // Assume we in a table or mobile
        $('html, body').animate({
          scrollTop: $("[data-id='"+toshow+"']").offset().top - 20
        }, 500);
      }
    }
  }
};

$(document).ready(function(){

  console.log("document ready");
  $('.site-loader').fadeOut("slow");

  // Top notices

  $(".userTool-notices").on("click", function() {
    if ($(".userTool-notices-list .notice").length > 0) {
      $(".userTool-notices-list").fadeToggle();
    }
  });

  // Top notification bar

  $('.topVerification .close-btn').on('click', function(){
    $(this).parents('.topVerification').slideUp();
  });

  // Selectric

  $(".select.only-dropdown select").selectric({
    disableOnMobile: false
  });

  $(".select:not(.only-dropdown) select").selectric({
    disableOnMobile: true
  });

  $("select.predictable").on("selectric-open", function(){
    if ($(this).parents(".selectric-wrapper").hasClass("selectric-above")) {
      $(this).parents(".select").siblings(".input").children(".label").addClass("transparent");
    }
  });

  $("select.predictable").on("selectric-close", function(){
    $(this).parents(".select").siblings(".input").children(".label").removeClass("transparent");
  });

  $(".profile-menu > .with-dropdown > a").on("click", function(e){
    e.preventDefault();
    $(this).parents(".with-dropdown").toggleClass("dropdown-collapsed");
  });

  // TODO: Rework this

  $("#hamburger").prop('checked', false);
  $("#hamburger").on('click', function () {
      $(".menu").toggleClass("fixed");
      $("#hamburger ~ nav ").toggleClass("openMenu");
      $("html").toggleClass("scrolllock");
      $(".autentification").css({'display':'none'});
  });

  // Selects with prediction

  $(".input.predictable input").each(function(){
    var input = $(this);
    var prefix = $(this).data("prefix_name");
    var prefix_select = $("[name='"+prefix+"']");
    prefix_select.on("change", function(){
      input.val("");
    });
  });

  $(".input.predictable input").on("input", function(){

    var name = $(this).attr("name");
    var select = $("select[name='"+name+"_select']");

    var input = $(this);
    var input_value = input.val();

    var prefix = $(this).data("prefix_name");
    if (prefix) {
      var prefix_select = $("[name='"+prefix+"']");
      var prefix_value = prefix_select.children("option:selected").text();
    } else {
      var prefix_value = "";
    }

    GoogleAutocomplete(input_value, prefix_value, select);
  });

  $(".select.only-dropdown select").on("change", function(){
    var name = $(this).attr("name").replace("_select", "");
    var input = $("input[name='"+name+"']");
    input.val($(this).children("option:selected").attr("real_name"));
  });

  // Datepickers

  var datepickers = {};
  var fields = ["birthdate", "passport_issued_at"];
  for (var field in fields) {
    var field_name = fields[field];
    if ($("[name='"+field_name+"']").length > 0) {
      datepickers[field_name] = new pikadayResponsive($("[name='"+field_name+"']")[0], {
        placeholder: "Выберите дату",
        pikadayOptions: {
          firstDay: 1,
          yearRange: [1900, new Date().getFullYear()],
          maxDate: new Date(),
          i18n: {
            previousMonth : 'Предыдущий Месяц',
            nextMonth     : 'Следующий Месяц',
            months        : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            weekdays      : ['Воскресение','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
            weekdaysShort : ['Вс','Пн','Вт','Ср','Чт','Пт','Сб']
          }
        }
      });
    }
  }

  // Tabs

  $("[data-toshow]").on("click", function(){
    var toshow = $(this).data("toshow");
    tugush.UI.goToTab(toshow);
  });

  // Show tab from hash

  if (location.hash) {
    var toshow = location.hash.replace("#", "");
    tugush.UI.goToTab(toshow);
  }

  // Revealable things

  $("[data-toreveal]").on("click", function(){
    var toreveal = $(this).data("toreveal");
    if ($(this).parents(".hideable-content")) {
      $(this).parents(".hideable-content").slideUp();
    } else {
      $(this).slideUp();
    }
    $("[data-id='"+toreveal+"']").slideDown();
  });

  //Автодополнение города в agreement2
  
  if ($('[name=sity2_select]').length)
  {
  	$('head').append('<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATGXhR_hCVEsmqcgMcwq0L9g_Nh1gqUh4&libraries=places"></script>');
  }
  $('[name=sity2]').on('input', function(e) {
    var input_value = this.value,
    		select = $('[name=sity2_select]'); 
    GoogleAutocomplete(input_value, null, select);
  });
  $('[name=sity2_select]').selectric();
  $('[name=sity2_select]').on('change', function(e) {
  		var value = this.options[this.selectedIndex].getAttribute('real_name');
  		$(this).closest('.input-container').find('> .input').val(value);
  })
});

$(window).on("load", function() {
  $(".overlay_preloader").fadeOut(1000);
});

//Подстановка автозаполнения от Гугла
function GoogleAutocomplete(input_value, prefix, select) {
  if (!prefix)
    prefix = 'Россия';
  if (input_value) {
    var service = new google.maps.places.AutocompleteService();
    service.getQueryPredictions(
      { input: prefix+" "+input_value, language: "ru"},
      function(results, status){
        var buffer = [];

        for (var idx in results) {
          var place = results[idx];
          if ("types" in place && place.types[0] == "locality") {
            buffer.push({id: place.place_id, name: place.terms[0].value, extra: place.terms[1].value});
          }
        }

        select.html("");
        for (var idx in buffer) {
          var place = buffer[idx];
          select.append($("<option>").val(place.id).attr("real_name", place.name).html(place.name+" <span>"+place.extra+"</span>"));
        }
        if (buffer.length < 1) {
          select.html("");
        }
        select.selectric("refresh");
        select.selectric("open");
      }
    );
  }
}
