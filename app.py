from apps import app

app.jinja_env.add_extension('pyjade.ext.jinja.PyJadeExtension')

if __name__ == '__main__':
    app.run(host='127.0.0.1', debug=True)
